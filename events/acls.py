import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state}",
        "per_page": 1,
        "page": 1,
        "orientation": "landscape",
    }
    response = requests.get(url, headers=headers, params=params)
    data = response.json()

    if data["photos"]:
        # return {"picutre_url": data["photos"][0]["src"]["original"]}
        return data["photos"][0]["src"]["original"]
    else:
        return None


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    geocoding_params = {
        "q": f"{city},{state}",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }

    # Make the request
    geocoding_response = requests.get(geocoding_url, params=geocoding_params)
    geocoding_data = geocoding_response.json()

    if not geocoding_data:
        return None

    # Get the latitude and longitude from the response
    latitude = geocoding_data[0]["lat"]
    longitude = geocoding_data[0]["lon"]

    # Create the URL for the current weather API with the latitude and longitude
    weather_url = "https://api.openweathermap.org/data/2.5/onecall"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "exclude": "minutely,hourly,daily,alerts",
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    # Make the request
    weather_response = requests.get(weather_url, params=weather_params)
    weather_data = weather_response.json()

    # Get the main temperature and the weather's description
    current_weather = weather_data["current"]
    main_temp = current_weather["temp"]
    weather_description = current_weather["weather"][0]["description"]

    # Put them in a dictionary and return it
    return {"temperature": main_temp, "description": weather_description}
