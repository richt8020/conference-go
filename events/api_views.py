from django.http import JsonResponse

from .models import Conference, Location

from common.json import ModelEncoder

import json

from .models import State

from django.views.decorators.http import require_http_methods

from .acls import get_photo, get_weather_data


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


@require_http_methods(["GET"])
def api_updatepics(request):
    locations = Location.objects.all()
    for loc in locations:

        loc.picture_url = get_photo(loc.city, loc.state.abbreviation)
        loc.save()

    return JsonResponse({"status": "success"})


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences}, encoder=ConferenceListEncoder
        )
    else:
        content = json.loads(request.body)
        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

    conference = Conference.objects.create(**content)

    location = conference.location
    weather_data = get_weather_data(location.city, location.state.abbreviation)
    conference_data = json.loads(
        JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        ).content
    )
    response_data = {"weather": weather_data, "conference": conference_data}
    return JsonResponse(response_data)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        location = conference.location
        weather_data = get_weather_data(
            location.city, location.state.abbreviation
        )
        conference_data = json.loads(
            JsonResponse(
                conference, encoder=ConferenceDetailEncoder, safe=False
            ).content
        )
        response_data = {
            "weather": weather_data,
            "conference": conference_data,
        }

        return JsonResponse(response_data)
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if not Location.objects.filter(id=content["location"]).exists():
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        else:
            Conference.objects.filter(id=id).update(**content)
            conference = Conference.objects.get(id=id)
            return JsonResponse(
                conference, encoder=ConferenceDetailEncoder, safe=False
            )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)

        location = Location.objects.create(**content)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

    # new code
    Location.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
